FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /test_simple
WORKDIR /test_simple
COPY Gemfile /test_simple/Gemfile
COPY Gemfile.lock /test_simple/Gemfile.lock
RUN bundle install
COPY . /test_simple
